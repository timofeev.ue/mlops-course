"""Start module."""

from src.data.clean_data import clean_data
from src.data.select_region import select_region
from src.features.add_features import add_features
import src.utils.log as log


RAW_DATA_PATH = "data/raw/all_v2.csv"
REGIONAL_DATA_PATH = "data/interim/data_regional.csv"
CLEANED_DATA_PATH = "data/interim/data_cleaned.csv"
FEATURED_DATA_PATH = "data/processed/data_featured.csv"
REGION_ID = 2661

if __name__ == "__main__":
    log.log_setup('DEBUG')
    select_region(RAW_DATA_PATH, REGIONAL_DATA_PATH, REGION_ID)
    clean_data(
        REGIONAL_DATA_PATH,
        CLEANED_DATA_PATH,
        min_area=15,
        max_area=300,
        min_price=1_000_000,
        max_price=100_000_000,
        min_kitchen=3,
        max_kitchen=70,
    )
    add_features(CLEANED_DATA_PATH, FEATURED_DATA_PATH)
