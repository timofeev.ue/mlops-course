"""Filter input dataset by region.

Returns:
    Filtered IN_PATH data by REG_ID into OUT_PATH
"""

import click
import pandas as pd

from src.utils.utils import to_abs_path
import src.utils.log as log


def select_region(in_path: str, out_path: str, reg_id: int) -> None:
    """Filter input dataset by region.

    Function selects the listings belonging to a specified region.
    After that result saved into `out_path` with csv format.

    Args:
        in_path (str): path to raw data in csv
        out_path (str): path to cleaned data in csv
        reg_id (int): region id for filtration data
    """
    logger = log.get_logger()
    logger.info("start select data")
    in_path, out_path = to_abs_path(in_path, out_path)
    logger.debug("input file: %s", *to_abs_path(in_path))
    logger.debug("output file: %s", *to_abs_path(out_path))
    logger.debug("params: %s", f"{reg_id=}")

    df = pd.read_csv(in_path)
    logger.debug(f"input data loaded, rows count = {df.index.size:,}")
    df = df[df["region"] == reg_id]
    df.drop("region", axis=1, inplace=True)

    logger.info(f"Selected {len(df)} samples in region {reg_id}.")
    logger.debug("flushing data to disk")
    df.to_csv(out_path)
    logger.info("task compleat")


@click.command()
@click.argument("in_path", type=click.Path(exists=True, readable=True))
@click.argument("out_path", type=click.Path(exists=False))
@click.option("--reg_id", "-r", default=2661, help="ID of region")
@click.option(
    "--log", "-l",
    default="info",
    type=click.Choice(
        ["critical", "error", "warn", "warning", "info", "debug"], case_sensitive=False
    ),
    show_default=True,
)
def cli(**kwargs):
    """Filter input dataset by region."""
    # extract and remove "log" from kwargs otherwise error in next call
    log.log_setup(kwargs.pop("log"))
    select_region(**kwargs)


if __name__ == "__main__":
    cli()
