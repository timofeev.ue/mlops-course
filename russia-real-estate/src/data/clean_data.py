"""Clear raw data."""

import click
import pandas as pd

from src.utils.utils import to_abs_path
import src.utils.log as log

MIN_AREA = 15  # Outlier range for floor area
MAX_AREA = 300

MIN_KITCHEN = 3  # Outlier range for kitchen area
MAX_KITCHEN = 70

MIN_PRICE = 1_000_000  # Outlier range for price
MAX_PRICE = 100_000_000


def clean_data(
    in_path: str,
    out_path: str,
    min_area: int = MIN_AREA,
    max_area: int = MAX_AREA,
    min_price: int = MIN_PRICE,
    max_price: int = MAX_PRICE,
    min_kitchen: int = MIN_KITCHEN,
    max_kitchen: int = MAX_KITCHEN,
) -> None:
    """Clean raw data.

    Function removes excess columns and enforces correct data types.

    Args:
        in_path (str, mandatory): path to raw data in csv
        out_path (str, mandatory): path to cleaned data in csv
        min_area (int): Outlier range for floor area (MIN)
        max_area (int): Outlier range for top area (MAX)
        min_price (int): Outlier range for floor price (MIN)
        max_price (int): Outlier range for top price (MAX)
        min_kitchen (int): Outlier range for floor kitchen are (MIN)
        max_kitchen (int): Outlier range for top kitchen area (MAX)
    """
    logger = log.get_logger()
    logger.info("start to making cleaned dataset from raw data")
    logger.debug("input file: %s", *to_abs_path(in_path))
    logger.debug("output file: %s", *to_abs_path(out_path))
    logger.debug(
        "params: %s",
        f"{min_area=}, {max_area=}, "
        f"{min_price=}, {max_price=}, "
        f"{min_kitchen=}, {max_kitchen=}",
    )

    df = pd.read_csv(in_path)
    logger.debug(f"raw data loaded, rows count = {df.index.size:,}")
    df.drop("time", axis=1, inplace=True)
    df["date"] = pd.to_datetime(df["date"])
    # Column actually contains -1 and -2 values presumably for studio apartments.
    df["rooms"] = df["rooms"].apply(lambda x: 0 if x < 0 else x)
    df["price"] = df["price"].abs()  # Fix negative values
    # Drop price and area outliers.
    df = df[(df["area"] <= max_area) & (df["area"] >= min_area)]
    df = df[(df["price"] <= max_price) & (df["price"] >= min_price)]
    # Fix kitchen area outliers.
    # At first, replace all outliers with 0.
    df.loc[
        (df["kitchen_area"] >= max_kitchen) | (df["area"] <= min_area), "kitchen_area"
    ] = 0
    # Then calculate kitchen area based on the floor area, except for studios.
    area_mean, kitchen_mean = df[["area", "kitchen_area"]].quantile(0.5)
    kitchen_share = kitchen_mean / area_mean
    df.loc[(df["kitchen_area"] == 0) & (df["rooms"] != 0), "kitchen_area"] = (
        df.loc[(df["kitchen_area"] == 0) & (df["rooms"] != 0), "area"] * kitchen_share
    )
    logger.info(f"all transformations is done, rows count = {df.index.size:,}")
    logger.debug("flushing data to disk")
    df.to_csv(out_path)
    logger.info("task compleat")


@click.command()
@click.argument("in_path", type=click.Path(exists=True, readable=True))
@click.argument("out_path", type=click.Path(exists=False))
@click.option(
    "--min_area",
    default=MIN_AREA,
    type=click.INT,
    show_default=True,
    help="Outlier range for floor area (MIN)",
)
@click.option(
    "--max_area",
    default=MAX_AREA,
    type=click.INT,
    show_default=True,
    help="Outlier range for top area (MAX)",
)
@click.option(
    "--min_price",
    default=MIN_PRICE,
    type=click.INT,
    show_default=True,
    help="Outlier range for floor price (MIN)",
)
@click.option(
    "--max_price",
    default=MAX_PRICE,
    type=click.INT,
    show_default=True,
    help="Outlier range for top price (MAX)",
)
@click.option(
    "--min_kitchen",
    default=MIN_KITCHEN,
    type=click.INT,
    show_default=True,
    help="Outlier range for floor kitchen are (MIN)",
)
@click.option(
    "--max_kitchen",
    default=MAX_KITCHEN,
    type=click.INT,
    show_default=True,
    help="Outlier range for top kitchen area (MAX)",
)
@click.option(
    "--log",
    "-l",
    default="info",
    type=click.Choice(
        ["critical", "error", "warn", "warning", "info", "debug"], case_sensitive=False
    ),
    show_default=True,
)
def cli(**kwargs):
    """Clean raw data.

    Function removes excess columns and enforces correct data types.
    """
    # extract and remove "log" from kwargs otherwise error in next call
    log.log_setup(kwargs.pop("log"))
    clean_data(**kwargs)


if __name__ == "__main__":
    cli()
