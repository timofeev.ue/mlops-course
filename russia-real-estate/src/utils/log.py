"""Utils for logging."""

import logging
from src.utils.utils import self_name


def log_setup(log_lvl: str = "INFO") -> None:
    """Invoke setting of logging subsystem.

    Args:
        log_lvl (str, optional): Logging level. Defaults to 'INFO'.
    """
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=log_lvl.upper(), format=log_fmt)


def get_logger(name: str = None) -> logging.Logger:
    """Return logger.

    Returns:
        logging.Logger: _description_
    """
    if not name:
        name = self_name(stack_lvl=2)
    return logging.getLogger(name)
